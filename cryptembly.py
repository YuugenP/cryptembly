import sys
args = sys.argv[1:]
filename = ""
_FLAGLIST = {
    "HELP": False,
    "VERBOSE": False
}

databaselist = {}
mainlist = []
pointer = []

class CryptError(Exception):
    pass

def showhelp():
    helpstring = '''
This is just a test.
lmao
''' 
    print (helpstring)
    return

def parse(filename, v):
    fname = filename
    with open(filename) as file:
        content = file.readlines()
        if v: print("Opened file: {0}".format(fname))
    
    content = [line.rstrip("\n") for line in content]
    status = None
    linecount = 0

    for line in content:
        linecount += 1
        if line.startswith("#"):
            if v: print("Comment found on line {0}, skipping.".format(linecount))
            pass
        elif line.lower().startswith("sect db:"):
            status = "db"
            if v: print("Now turning reading head towards db at Line {0}".format(linecount))
            pass
        elif line.lower().startswith("dec"):
            dec(line, status, v, linecount)
        # MAIN
        elif line.lower().startswith("sect main:"):
            status = "main"
            if v: print("Now turning reading head towards main at Line {0}".format(linecount))
            pass
        elif line.lower().startswith("mov"):
            if 


# COMMANDS DECLARATION

'''
DEC

Declares a variable

dec <type> <name> <val>
'''

def dec(line, status, v, linecount):
    if status == "db":
        arg = line.lower().split(" ")
        if len(arg) < 4:
            raise CryptError("Missing Argument(s) to variable. Are you sure it's following the [dec <type> <name> <val>] format?")
        else:    
            flag = False
            for i in ["str", "int", "char"]:
                if arg[1] == i:
                    flag = True
            if flag:
                databaselist[arg[2]] = {
                    "type": arg[1],
                    "value": arg[3]
                }
                if v: print ("Variable Declaration at {0}: {1}".format(linecount, line))
                print (databaselist)
            else:
                raise CryptError("Unknown Variable type. Please make sure it's either str, int or char.")
    else:
        raise CryptError("Declaration of variable outside database section")

def vig(ende, string, key):
    result = []
    for i in range(len(string)):
        key_c = key[i % len(key)]
        if ende == "en":
            res_c = chr(ord(string[i]) + ord(key_c) % 256)
        else:
            res_c = chr((ord(string[i]) - ord(key_c) + 256) % 256)
        result.append(res_c)
    
    return ''.join(result)

parse("./test.cry", True)

